![](https://cdn.iconscout.com/icon/free/png-256/bootstrap-7-1175254.png)
# Bootststrap for noobs

>Template para iniciar en bootstrap. Es un esqueleto con los elementos más utilizados en cualquier proyecto de bootstrap, puede utilizarse con fines didácticos o laborales.

El objetivo sería hacer un code along donde los participantes puedan replicar este template en una hora. Para ello es necesario tener conocimientos básicos de **HTML** y **CSS**.

#### Contenido
- HTML Básico
- Hoja de estílos
- No requiere instalar bootstrap
- No requiere instalar jquery

#### Referencia y documentación en:
- [Sitio oficial de Bootstrap](https://getbootstrap.com/)
#### Arquitectura:
La arquitectura es bastante sencilla

```sh
├── index.html
├── main.css
├── main.js

3 files
```
En ***index.html*** se encuentran agregadas las librerías requeridas a través de un CDN

*Realizado por Janio Isacura*